FROM java:8
ADD ./target/example.jar .
EXPOSE 8080
ENTRYPOINT exec /usr/bin/java -jar ./example.jar