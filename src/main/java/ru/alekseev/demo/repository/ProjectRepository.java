package ru.alekseev.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.alekseev.demo.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

}
