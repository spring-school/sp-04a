package ru.alekseev.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.alekseev.demo.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

}
